package com.invoice.venta.repository;

import com.invoice.venta.domain.mappers.Contacto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactoRepository extends CrudRepository<Contacto, Long> {

}
