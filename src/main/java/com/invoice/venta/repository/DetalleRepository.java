package com.invoice.venta.repository;

import com.invoice.venta.domain.mappers.Detalle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetalleRepository extends CrudRepository<Detalle, Long> {

}
