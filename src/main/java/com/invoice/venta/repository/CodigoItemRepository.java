package com.invoice.venta.repository;

import com.invoice.venta.domain.mappers.CodigoItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodigoItemRepository extends CrudRepository<CodigoItem, Long> {

}
