package com.invoice.venta.repository;

import com.invoice.venta.domain.mappers.Facturadorelectronico;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacturadorelectronicoRepository extends CrudRepository<Facturadorelectronico, Long> {

}
