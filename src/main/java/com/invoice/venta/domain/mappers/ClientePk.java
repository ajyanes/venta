package com.invoice.venta.domain.mappers;

import java.io.Serializable;

public class ClientePk implements Serializable {

  private String prefijo;
  private String numero;
}
