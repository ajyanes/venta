package com.invoice.venta.domain.mappers;

import java.io.Serializable;

public class FacturaPk implements Serializable {

  private String prefijo;
  private String numero;
}
