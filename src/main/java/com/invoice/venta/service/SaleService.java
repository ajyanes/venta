package com.invoice.venta.service;

import com.invoice.venta.domain.mappers.Factura;

public interface SaleService {

  Factura data();

}
