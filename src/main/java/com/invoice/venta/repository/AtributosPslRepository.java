package com.invoice.venta.repository;

import com.invoice.venta.domain.mappers.AtributosPsl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtributosPslRepository extends CrudRepository<AtributosPsl, Long> {

}
