package com.invoice.venta.controller;

import com.invoice.venta.domain.mappers.Factura;
import com.invoice.venta.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ventas")
public class SaleController {

  @Autowired
  private SaleService saleService;

  @GetMapping("/getdata")
  public ResponseEntity<Factura> getFactura() {
    return new ResponseEntity<>(saleService.data(), HttpStatus.ACCEPTED);
  }


}
