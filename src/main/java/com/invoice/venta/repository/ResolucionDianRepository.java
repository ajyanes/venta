package com.invoice.venta.repository;

import com.invoice.venta.domain.mappers.ResolucionDian;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResolucionDianRepository extends CrudRepository<ResolucionDian, Long> {

}
