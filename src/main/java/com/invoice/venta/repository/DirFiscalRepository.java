package com.invoice.venta.repository;

import com.invoice.venta.domain.mappers.DirFiscal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DirFiscalRepository extends CrudRepository<DirFiscal, Long> {

}
